import os
from slack_sdk import WebClient
import json

SLACK_TOKEN = os.getenv("SLACK_TOKEN")
PROJECT_URL = os.environ["CI_PROJECT_URL"]
SLACK_CHANNEL = os.environ["SLACK_CHANNEL"]

def send_failure_notification():
    print("PROJECT_URL")
    print(PROJECT_URL)

    print(os.environ);

    base_project_url = PROJECT_URL

    with open("leaks.json", "r") as f:
        data = json.load(f)

    count = len(data)

    project_name = os.environ["CI_PROJECT_NAME"]

    branch_name = os.environ["CI_COMMIT_BRANCH"]

    message = f"*{count} Secrets* found in *{project_name}*"

    commit_description = os.environ["CI_COMMIT_TITLE"]

    stage_name = os.environ["CI_JOB_STAGE"]

    job_name = os.environ["CI_JOB_NAME"]

    job_url = os.environ["CI_JOB_URL"]

    pipeline_url = os.environ["CI_PIPELINE_URL"]

    commit_id = os.environ["CI_COMMIT_SHA"]

    client = WebClient(token=SLACK_TOKEN)

    res = client.chat_postMessage(
        channel=SLACK_CHANNEL,
        attachments=[
            {
                "color": "red",
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "text": f"<{pipeline_url}|{message}>",
                            "type": "mrkdwn",
                        },
                        "fields": [
                            {"type": "mrkdwn", "text": "*Branch*"},
                            {"type": "mrkdwn", "text": "*Commit*"},
                            {"type": "mrkdwn", "text": f"<{base_project_url}/tree/{branch_name}|{branch_name}>"},
                            {
                                "type": "mrkdwn",
                                "text": f"<{base_project_url}/-/commit/{commit_id}|{commit_description}>",
                            },
                            {"type": "mrkdwn", "text": "*Stage*"},
                            {"type": "mrkdwn", "text": "*Job*"},
                            {"type": "mrkdwn", "text": f"{stage_name}"},
                            {"type": "mrkdwn", "text": f"<{job_url}|{job_name}>"},
                        ],
                    }
                ],
            }
        ],
    )

    res = client.files_upload(
        channels=[SLACK_CHANNEL],
        file="leaks.json",
        title="Redacted Sensitive Information",
        initial_comment="Sensitive data in the code",
    )


send_failure_notification()
